package fr.cegid.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.cegid.beans.User;
import fr.cegid.utils.DatabaseConnexion;

public class UserConnection {
	
	public static boolean getConnection(User user, Connection cnx) throws SQLException {
		PreparedStatement rqt = null;
		ResultSet rs = null;
		boolean bool = false;
		
		try {
			rqt = cnx.prepareStatement("select uemail, upass from user_reg where uemail=? and upass=?");
			rqt.setString(1, user.getEmail());
			rqt.setString(2, user.getPassword());
			rs = rqt.executeQuery();

			if(rs.next()) {
				bool = true;
			}
			
		} finally {
			if (rqt!=null) rqt.close();
			if (cnx!=null) cnx.close();
		}
		return bool;
	}
}
	
	


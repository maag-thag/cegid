package fr.cegid.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.cegid.beans.User;
import fr.cegid.dal.UserConnection;
import fr.cegid.forms.ConnexionForm;
import fr.cegid.utils.DatabaseConnexion;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Pr�paration de l'objet formulaire */
        ConnexionForm form = new ConnexionForm();
		
        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        User user = form.connectUser(request); 

        /* R�cup�ration de la session depuis la requ�te */
        HttpSession session = request.getSession();

	    try {
	    	Connection con = DatabaseConnexion.getConnection();
	    	boolean isReadyToConnect = UserConnection.getConnection(user, con);
	    	
			if(isReadyToConnect) {
				/* permet de s'assurer que le nom est �crit en majuscule */
				String firstNameMaj = request.getParameter("firstName").substring(0, 1).toUpperCase() + request.getParameter("firstName").substring(1);
				session.setAttribute("firstName", firstNameMaj);  
				response.sendRedirect("http://localhost:8080/cegid/Home?firstName=" + firstNameMaj);
				
			} else {
				request.setAttribute("error", true);
				this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
			}
	    } catch (SQLException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }
		
	}
	
		

}

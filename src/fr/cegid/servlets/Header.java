package fr.cegid.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Header
 */
@WebServlet("/Header")
public class Header extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Header() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("menu");
		
		if(param != null && param != "") {
			if(param.equals("quisuisje")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/presentation.jsp").forward(request, response);
			}
			if(param.equals("cegidmeplait")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/cegidmeplait.jsp").forward(request, response);
			}
			if(param.equals("vousconvaincre")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/presentation.jsp").forward(request, response);
			}
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

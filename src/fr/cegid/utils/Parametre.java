package fr.cegid.utils;

import java.util.ResourceBundle;

public class Parametre {
	
	public static String lire(String cle){
		ResourceBundle bundle = ResourceBundle.getBundle("fr.cegid.utils.param");
		
		return (null!=bundle) ? bundle.getString(cle) : null;
	}

}

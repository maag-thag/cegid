package fr.cegid.beans;

public class User {

    private String email;
    private String password;
    private String firstName;

	public User(String email, String password, String firstName) {
		super();
		this.email = email;
		this.password = password;
		this.firstName = firstName;
	}
	
	public User(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}

<%@ include file="doInitPage.jsp" %>
<link rel="stylesheet" href="/cegid/cegid-app/style/css/icomoon/style.css" type="text/css">
<link rel="stylesheet" href="/cegid/cegid-app/style/css/presentation.css" type="text/css">

<%@ include file="doHeader.jsp" %>
<div class="container-fluide">
	<div class="container">
		<div class="row top-page">
			<img src="/cegid/cegid-app/img/photo-thomas.png" alt="photo-thomas" class="photo" />
			<div class="title-presentation">
				<h3>Thomas Aguirregabiria</h3>
				<h4>D�veloppeur Fullstack</h4>	
			</div>
		</div>
	</div>
</div>
<div class="container-fluide presentation">
	<div class="container">
		<div class="row main-content">
			<div class="global-presentation">
				<p>Apr�s diverses exp�riences professionnelles durant lesquelles je me suis initi� � l'univers du d�veloppement web,
				je d�cide en 2016 de suivre formation Concepteur D�veloppeur Informatique de l'ENI (bac+4). Apr�s un stage r�ussi de 2 mois au sein de la soci�t� 
				Alteca, je rejoins la soci�t� ASI qui correspond davantage � mes ambitions technologiques.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluide all-cards">
	<div class="container">
		<div class="row main-content">
			<div class="card-content">
				<div class="card card-techno " id="card">
					<figure class="front">
						<H3>Technologies</H3>
						<span class="icomoon icon-cogs"></span>
						<div class="button">D�couvrir</div>
					</figure>
					<figure class="back">
						<h4>Technologies privil�gi�es</h4>
						<p>La liste ci-dessous refl�te � la fois mon expertise actuelle et celle sur laquelle je souhaite m'orienter dans les ann�es � venir</p>
						<ul>
							<li>Java 8</li>
							<li>Javascript</li>
							<li>React</li>
							<li>VueJS</li>
							<li>SQL</li>
							<li>CSS</li>
							<li>HTML</li>
						</ul>
						<p>Je me consid�re comme un d�veloppeur fullstack. Mes connaissances techniques sont � la fois utiles � la mise en place d'algorithmes et d'architectures complexes, que pour la cr�ation d'interfaces design et optimis�es � l'exp�rience utilisateur.</p>
					</figure>
				</div>
				<div class="card card-asi" id="card2">
					<figure class="front">
						<H3>Soci�t� - ASI</H3>
						<span class="icomoon icon-office"></span>
						<div class="button">D�couvrir</div>
					</figure>
					<figure class="back">
						<h4>ASI - ESN</h4>
						<p>Au sein d'une �quipe compos�e d'une vingtaine de d�veloppeurs de tous niveaux, je d�veloppe des espaces collaboratifs en Java 7 et Java 8. Nous utilisons le CMS fran�ais JCMS qui offre une s�curit� et une prise en main
						appr�ci�e par nos clients. J'utilise �galement Javascript (JQuery, AngularJS), XML, HTML5, Less (CSS3). Depuis mon arriv�e, j'ai travaill� pour les clients suivants :
							<ul>
								<li>Arkea (Cr�dit Mutuel)</li>
								<li>MAIF</li>
								<li>Logilec</li>
								<li>SNI</li>
								<li>Keolis</li>
								<li>SDIS81</li>
							</ul>
						</p>
						<p>Notre processus de travail correspond au fonctionnement DevOps. Je travaille en lien direct avec les clients et m'occupe �galement de l'exploitation des livrables. Nous utilisons Jenkins pour assurer une int�gration continue.
						En cas d'interrogation, chaque d�veloppeur peut se r�f�rer � des experts techniques, aussi bien pour nos d�veloppements que pour la mise en production et l'exploitation de nos applications.</p>
						<p>J'appr�cie cette fa�on de travailler. Elle nous offre une maitrise compl�te de toute la cha�ne de cr�ation du produit, ce qui nous rend autonome si n�cessaire. </p>
					</figure>
				</div>
				<div class="card card-futur" id="card3">
					<figure class="front">
						<H3>Avenir</H3>
						<span class="icomoon icon-make-group"></span>
						<div class="button">D�couvrir</div>
					</figure>
					<figure class="back">
						<h4>L'avenir</h4>
						<p>Mon caract�re curieux et passionn� me pousse tous les jours � d�couvrir les nouveaut�s du web. Je me fixe bien souvent des objetifs techniques sur mon temps libre. Je visionne et je lis de nombreux tutoriels le soir et le weekend.</p>
						<p>Mes ambitions se cr��es de cette mani�re et sont principalement techniques : l'envie d'acqu�rir une aisance parfaite de Java et des framework Javascript. Mes cibles sont ReactJS, NodeJS, VueJS et Java 8+.
						<p>Je n'exclus pas l'id�e d'un jour devenir chef de projet, scrum master ou bien m�me formateur. Je m'�panouie n�anmoins dans la d�couverte technique ce qui, pour le moment, ne m'encourage pas � prendre un tel virage professionnel.</p>
					</figure>
				</div>
			</div>
		</div>
	</div>
</div>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script>
$('.card').on('touchstart click',function(e) {
	e.preventDefault();
	$(this).toggleClass("flipped");
})
</script>

<%@ include file="doFooter.jsp" %>
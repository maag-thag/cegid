<%@ include file="doInitPage.jsp" %>
<link rel="stylesheet" href="/cegid/cegid-app/style/css/home.css" type="text/css">

<%@ include file="doHeader.jsp" %><%

request.getSession();
if (session == null) { 
	response.sendRedirect("Login"); 
} 

String name = request.getParameter("firstName") != null ? request.getParameter("firstName") : "" ;

%>

<div class="one-page" style="background-image: url('/cegid/cegid-app/img/slide-ballons-maison.jpg');">
	<div class="container">
		<section class="home-hang">
			<h2>Ma détermination est un atout qui va vous plaire </h2>
			<h2 class="orange"><%= name %></h2>
		</section>
		<section>
			<div>
				<!-- TODO -->
			</div>
		</section>
		<section class="home-item">
			<div>
				<!-- TODO -->
			</div>
		</section>

	</div>
</div>
<%@ include file="doFooter.jsp" %>
<%@ page contentType="text/html charset=UTF-8" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/cegid/cegid-app/style/css/login.css" type="text/css">
		<title>Cegid - votre prénom ?</title>
	</head>
	<body>
		<div class="bg-img">
			<div class="login-form-container">
				<img src="/cegid/cegid-app/img/cegid.svg" alt="logo Cegid" />
				<p>vs</p>
				<p>Thomas AGUIRREGABIRIA</p>
		        <form method="post" action="Login">
		            <input type="text" name="firstName" id="firstname" placeholder="Entrez votre prénom" onkeyup="hiddenHandler()">      
		            <input type="text" name="email" id="email" placeholder="Entrez votre e-mail" onkeyup="hiddenHandler()">
		            <input type="password" name="password" id="password" placeholder="Entrez votre mot de passe" onkeyup="hiddenHandler()">
		            <input type="submit" class="btn-submit" value="C'est parti !" onClick="return isValide();">
		        </form><%
		        if(request.getAttribute("error") != null) {
			        if(request.getAttribute("error").equals(true)) {%>
			        	<label id="error-empty-null" class="error errors-first-name">Erreur dans la soumission du questionnaire</label>
			        <%}
			    }%>
		      	<label hidden id="error-empty-null" class="error errors-first-name">Pour continuer, renseignez votre prénom</label>
	            <label hidden id="error-only-char" class="error errors-first-name">Merci de rentrer un prénom correct</label>
	            <label hidden id="error-too-long" class="error errors-first-name">C'est un peu long comme prénom, non ?</label>
	            <label hidden id="error-not-email" class="error">Merci de renseigner un email valide</label>
	            <label hidden id="error-not-password" class="error">Merci de mettre un mot de passe</label>
		    </div>
		</div>
	</body>
</html>

<script src="${pageContext.request.contextPath}/cegid-app/js/login.js"></script>
	


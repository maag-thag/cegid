</head>
<body>
<div class="my-container">
	<div class="logo">
		<h1>
			<a href="https://www.cegid.com/fr/" target="blank">
				<img src="/cegid/cegid-app/img/cegid.svg" alt="logo Cegid" />
			</a>
		</h1>
	</div>
	<nav>
		<div class="toggle">
			<span class="glyphicon glyphicon-menu-hamburger menu" aria-hidden="true"></span>
		</div>
		<div class="main-menu">
			<ul class="nav-list">
				<li><a href="Home">Accueil</a></li>
				<li><a href="Header?menu=quisuisje">Qui suis-je ?</a></li>
				<li><a href="Header?menu=cegidmeplait">Cegid me pla�t</a></li>
				<li><a href="Header?menu=vousconvaincre">Vous convaincre</a></li>
				<li><a href="Header?menu=contact">Contact</a></li>
			</ul>
		</div>
	</nav>
</div>

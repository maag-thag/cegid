<%@ page pageEncoding="UTF-8" %>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1" >
	<title>CEGID</title>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/cegid-app/style/css/common.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/cegid-app/style/css/header.css" type="text/css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">

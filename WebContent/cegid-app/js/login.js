

var errorLabels =  document.getElementsByClassName("error");
var inputFirstName = null;
var inputEmail = null;
var inputPwd = null;
var btnSubmit = null;

// Contrôle l'envoi du formulaire (onSubmit)
function isValide() {
	
	addHiddenOnErrorLabels();
	
	inputFirstName = document.querySelector("#firstname").value;
	inputEmail = document.querySelector("#email").value;
	inputPwd = document.querySelector("#password").value;

	btnSubmit = document.getElementsByClassName("btn-submit");
	
	if(!checkEmpty() || !checkTypeChar() || !checkLength() || !checkLogin()) {
		return false;		
	}
	return true;
};

// Optimise l'ux pour supprimer les messages d'erreurs (onkeyup)
function hiddenHandler() {
	if(inputFirstName != null) {
		if (inputFirstName.length == 0) {
			document.getElementsByClassName("errors-first-name").hidden = true;
		}
	}
	if(inputEmail != null) {
		var inputEmailLength = inputEmail.length;
	}
	if(inputPwd != null) {
		var inputPwdLength = inputPwd.length;
	}
	
	
	if(inputEmailLength == 0) {
		document.querySelector("#error-not-email").hidden = true;
	}
	if(inputEmailLength == 0) {
		document.querySelector("#error-not-password").hidden = true;
	}
};

// --------- Méthodes utilitaires --------------------------------

function checkEmpty() {
	if(inputFirstName == null | inputFirstName == '') {
		document.getElementById("error-empty-null").removeAttribute("hidden");
		return false;
	} if (inputPwd == null | inputPwd == '') {
		document.getElementById("error-not-password").removeAttribute("hidden");
		return false; 
	}  else {
		return true;
	}
}

function checkTypeChar() {
	if (!(/^[a-zA-ZéèîïÉÈÎÏ][a-zéèêàçîï]+([-'\s][a-zA-ZéèîïÉÈÎÏ][a-zéèêàçîï]+)?$/.test(inputFirstName))) {
		document.getElementById("error-only-char").removeAttribute("hidden");
		return false;
	} if (!(/^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/.test(inputEmail))) {
		document.getElementById("error-not-email").removeAttribute("hidden");
		return false; 
	} else {
		return true;
	}
}

function checkLength() {
	if(inputFirstName.length < 2 && inputFirstName.lenght > 30) {
		document.getElementById("error-too-long").removeAttribute("hidden");
		return false;
	} else {
		return true;
	}
}

function checkLogin () {
	if(inputPwd != "password") {
		document.getElementById("error-not-mdp").removeAttribute("hidden");
		return false; 
	} else {
		return true;
	}
}

function addHiddenOnErrorLabels() {
	for (var i = 0; i < errorLabels.length; i++) {
		errorLabels[i].hidden = true;
	}
}








